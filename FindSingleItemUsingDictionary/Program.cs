﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FindSingleItemUsingDictionary
{
    class Program
    {
        public static void Main()
        {
            int[] seq = { 1, 5, 4, 1, 5, 3, 7, 7, 3, 4, 8 };
            Console.WriteLine(FindSingle(seq));
        }

        public static int FindSingle(int[] Sequance)
        {
            Dictionary<int, int> Pairs = new Dictionary<int, int>();
            for (int i = 0; i < Sequance.Length; i++)
            {
                if (Pairs.ContainsKey(Sequance[i]))
                    Pairs[Sequance[i]]++;
                else
                    Pairs[Sequance[i]] = 1;
            }
            return Pairs.FirstOrDefault(item => item.Value == 1).Key;
        }
    }
}